const nhlTeams = [
    {
        name: 'Chicago Blackhawks',
        resident: 'Chicago, IL',
        state: 'US',
        conference: 'west',
        division: 'central',
    },
    {
        name: 'Pittsburgh Penguins',
        resident: 'Pittsburgh, PA',
        state: 'US',
        conference: 'east',
        division: 'capital',
    },
    {
        name: 'Anaheim Ducks',
        resident: 'Anaheim, CA',
        state: 'US',
        conference: 'west',
        division: 'pacific',
    },
    {
        name: 'Montreal Canadians',
        resident: 'Montreal, QC',
        state: 'CA',
        conference: 'east',
        division: 'atlantic',
    },
    {
        name: 'Anaheim Ducks',
        resident: 'Anaheim, WH',
        state: 'RU',
        conference: 'north',
        division: 'mars',
    },
]


//

let result = []
for ( let i = 0; i < nhlTeams.length; i++) {
    if (nhlTeams[i].name === 'Anaheim Ducks') {

        result.push(nhlTeams[i]);
    }
}
console.log(result)


// forEach loop------------------------------------

// nhlTeams.forEach(elem => {
//     if (elem.state === 'US')
//         console.log(elem.name)
//     })

// for loop------------------------------------

// for ( let i = 0; i < nhlTeams.length; i++) {
//     if (nhlTeams[i].state === 'US') {
//         console.log(nhlTeams[i].name)
//     }
// }


// Break  нахождение объекта по имени и прерывание цикла после нахождения---

// let result = null
// for ( let i = 0; i < nhlTeams.length; i++) {
//     if (nhlTeams[i].name === 'Anaheim Ducks') {
//         result = nhlTeams[i];
//         break
//     }
// }
// console.log(result)


// for in loop------------------------------------
// for ( let i in nhlTeams) {
//     console.log(nhlTeams[i].name)
// }


// for of loop------------------------------------
// for( let i of nhlTeams ) {
//     if (i.state === 'US') {
//         console.log(i.name)
//     }
// }


// while loop------------------------------------

// let i = 0;
//
// while ( i < nhlTeams.length ) {
//     if (nhlTeams[i].state === 'US') {
//         console.log(nhlTeams[i].name)
//     }
//     i ++;
// }
