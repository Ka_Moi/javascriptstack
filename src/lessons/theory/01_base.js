// const name = 'john'
// const age = 30
//
// console.log('name:' + name + ', age:' + age);
//
// const lastName = prompt('enter your last name')
// alert(name + ' ' + lastName)


//Операторы

// const currentYear = 2021;
// const birthYear = 2020;
//
// let age = currentYear - birthYear
// console.log(--age)
// ++ - увеличить на 1
// -- - уменьшить на 1

//
// let a = 10;
// let b = 6;
//
// (a += b)
// console.log(a)

// Приоритет операторов - MDN operators list
// const fullAge = 30
// const birthYear = 1990
// const current = 2020
//
// const isFullAge = current - birthYear >= fullAge
// console.log(isFullAge)



// Условные операторы
// const courseStatus = 'ready' // ready, fail, pending
//
// if  (courseStatus === 'ready') {
//     console.log('готово')
// } else if (courseStatus === 'pending') {
//     console.log('в процессе')
// } else {
//     console.log('fail')
// }

// const isReady = true

// if (isReady) {
//     console.log('done')
// } else {
//     console.log('undone')
// }

// Тернарное выражение

// isReady ? console.log('всё готово') : console.log('всё не готово')

// Строгое сравнение из-за типов данных, всегда пиши  ===

// const num1 = 42;
// const num2 = '42';
//
// console.log(num1 === num2)

// Функции

// function calculateAge(year) {
//     return 2020 - year
// }
//
// function logInfoAbout(name, year) {
//     const age = calculateAge(year)
//
//     if (age > 0) {
//         console.log('Человек' + name + ' возраст' + age)
//         } else {
//         console.log('Это будущее!')
//     }
// }
//
// logInfoAbout('Мужик', '2022')

// Массивы

// const originalSix = ['Boston', 'New York', 'Detroit', 'Toronto', 'Montreal', 'Chicago'];
//
// for (let i = 0; i < originalSix.length; i++) {
//     const six = originalSix[i]
//     console.log(six)
// }
//
// // for (let six of originalSix) {
// //     console.log(six)
// // }
//
// console.log(originalSix);

// Объекты

// const person = {
//     firstName: 'John',
//     lastName: 'Johns',
//     year: 1992,
//     languages: ['Ru', 'En'],
//     hasWife: false
// }
//
// console.log(person.year);
// console.log(person['lastName']);
// const key = 'hasWife';
// console.log(person[key]);
// person.isPrototype = true;
// console.log(person)
