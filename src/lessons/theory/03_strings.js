// const name = 'john'
// const age = 26;
//
// function getAge() {
//     return age
// }


// const output = 'Hello my name is ' + name + 'and i am' + age + ' years old'
// const output = `Hi my is ${name} and i am ${age < 27 ? 'a' : 'b'} years old`
// console.log(output);

// const output = `
//     <div>This is div</div>
//     <p>this is p</p>
// `
// console.log(output);

// const name = 'John'
// console.log(name.length);
// console.log(name.toUpperCase());
// console.log(name.toLowerCase());
// console.log(name.charAt(2)); //позиция символа в строке
// console.log(name.toLowerCase().startsWith('jo')); // искомая строка сначала приведена к строчному варианту
// console.log(name.startsWith('Jo')); // начинается ли строка с искомого выражения
// console.log(name.repeat(3));
// const string = '   lock    '
// console.log(string.trimLeft());

// function  logPerson(s, name, age) {
//     return `${s[0]}${name}${s[1]}${age}${s[2]}`
// }
//
// const personName = 'John'
// const personAge = '30'
//
// const result = logPerson`Имя: ${personName}, Возраст: ${personAge}`
// console.log(result);


// const obj1 = {
//     name: 'carl',
//     age: 90,
// }
// const obj2 = {
//     name: 'ben',
//     age: 80,
// }
//
// const objArray = [{obj1}, {obj2} ]
// console.log(objArray);


// const array = [
//     {
//         user: 'vitya',
//         parole: '143621'
//     },
//     {
//         user: 'vanya',
//         parole: '546267'
//     },
//     {
//         user: 'kolya',
//         parole: '435756'
//     },
// ]
//
// const vitya = array[0]
//
// console.log(vitya);


// Шаблонные строки (template literals)

// const user = 'Bob';
// const num = 17;
//
//
// const txt = 'Hello, ' + user + ' you have ' + num + ' letters in your inbox';
//
// const txt2 = `Hello, ${user} you have ${num} letters in your inbox`
//
// console.log(txt2)

// const html =
//     '<ul>' +
//     '<li>Item One</li>' +
//     '<li>Item Two</li>' +
//     '</ul>';
//
// const items = ['tea', 'tea2']
//
// const templateHtml = `
// <ul>
//     <li>${items[0]}</li>
//     <li>${items[1]}</li>
// </ul>`
//
// console.log(templateHtml)
