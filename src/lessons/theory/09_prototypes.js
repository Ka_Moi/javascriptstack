// Прототипы и классы
// ===========
//
//
// Создание прототипа с использованием метода Object.create
// const animal = {
//     say: function () {
//         console.log(this.name, 'goes', this.voice)
//     }
// };
//
// function createAnimal(name, voice) {
//     const res = Object.create(animal);
//     res.name = name;
//     res.voice = voice;
//     return res;
// }
//
// const dog = createAnimal('Dog', 'woof');
// const cat = createAnimal('Cat', 'meow');
//
// dog.say();
// cat.say();
//
// Создание прототипа с использованием ключевого слова New
//
// function Animal(name, voice) {
//     this.name = name;
//     this.voice = voice;
// }
//
// Animal.prototype.say = function() {
//     console.log(this.name, 'goes', this.voice);
// };
//
// const dog = new Animal('Dog', 'woof');
// dog.say();
//
// Классы
//
// class Animal {
//     constructor(name, voice) {
//         this.name = name;
//         this.voice = voice;
//     }
//     say() {
//         console.log(this.name, 'goes', this.voice)
//     }
// ;}
//
//
// // Функция constructor задает свойства для будущих классов, или добавляет свойства к действующим
// // Ключевое слово super нужно, чтобы передать свойства конструктора суперкласса, т.е. - вышестоящего, или прототипа
//
// class Bird extends Animal {
//     constructor(name, voice, canFly) {
//         super(name, voice);
//         super.say()
//         this.canFly = canFly;
//     }
//
//     say() {
//         console.log('Birds don`t like to talk')
//     }
// }
//
// const duck = new Bird('duck', 'quack', true)
// duck.say();
//
//
// class Animal {
//     constructor() {}
//
//     sayHello() {}
// }
//
// class Bird extends Animal {
//     constructor() {
//         super();
//     }
//
//     sayHello() {
//         return 'Pew pew';
//     }
// }
//
// class Cat extends Animal {
//     constructor() {
//         super();
//     }
//
//     sayHello() {
//         return 'Mew mew';
//     }
// }
//
//
// function makeTheAnimalTalk(animal) {
//     if (animal instanceof Animal) {
//         console.log(animal.sayHello());
//     }
// }
//
//
// makeTheAnimalTalk(new Cat());
//
// makeTheAnimalTalk(new Bird());
//
