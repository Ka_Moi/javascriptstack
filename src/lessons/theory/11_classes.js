// Ex1 Creating a new class (declaration-form)
// ===========================================

class Polygon {
    constructor(height, width) {
        this.name = 'Polygon';
        this.height = height;
        this.width = width;
    }

    sayName() {
        console.log('Hi, i am a ', this.name + '.');
    }

    sayHistory() {
        console.log('"Polygon is derived from the Greek polus (many)" ' + 'and gonia (angle).')
    }
}

let p = new Polygon(300, 400);
p.sayName();
console.log('The width of this polygon is ' + p.width);

// Ex2 Creating a new class (expression-form)
// ==========================================


const MyPoly = class Poly {
    getPolyName() {
        console.log('Hi. I was created with a Class expression. My name is ' + Poly.name);
    }
};

let inst = new MyPoly();
inst.getPolyName();


// Ex3 Extending an existing class
// ===============================

class Square extends Polygon {
    constructor(length) {
        super(length, length);
        this.name = 'Square';
    }

    get area() {
        return this.height * this.width;
    }
}

let s = new Square(5);

s.sayName();
console.log('The area of this square is ' + s.area);


// Ex4 Subclassing methods of a parent class
// =========================================



// Ex5 Creating a new class (functional declaration)
function Person(first, last, age, gender, interests) {
    this.name = {
        'first': first,
        'last' : last
    };
    this.age = age;
    this.gender = gender;
    this.interests = interests;
    this.bio = function() {
        // First define a string, and make it equal to the part of
        // the bio that we know will always be the same.
        var string = this.name.first + ' ' + this.name.last + ' is ' + this.age + ' years old. ';
        // define a variable that will contain the pronoun part of
        // the second sentence
        var pronoun;

        // check what the value of gender is, and set pronoun
        // to an appropriate value in each case
        if(this.gender === 'male' || this.gender === 'Male' || this.gender === 'm' || this.gender === 'M') {
            pronoun = 'He likes ';
        } else if(this.gender === 'female' || this.gender === 'Female' || this.gender === 'f' || this.gender === 'F') {
            pronoun = 'She likes ';
        } else {
            pronoun = 'They like ';
        }

        // add the pronoun string on to the end of the main string
        string += pronoun;

        // use another conditional to structure the last part of the
        // second sentence depending on whether the number of interests
        // is 1, 2, or 3
        if(this.interests.length === 1) {
            string += this.interests[0] + '.';
        } else if(this.interests.length === 2) {
            string += this.interests[0] + ' and ' + this.interests[1] + '.';
        } else {
            // if there are more than 2 interests, we loop through them
            // all, adding each one to the main string followed by a comma,
            // except for the last one, which needs an and & a full stop
            for(var i = 0; i < this.interests.length; i++) {
                if(i === this.interests.length - 1) {
                    string += 'and ' + this.interests[i] + '.';
                } else {
                    string += this.interests[i] + ', ';
                }
            }
        }

        // finally, with the string built, we alert() it
        alert(string);
    };
    this.greeting = function() {
        alert('Hi! I\'m ' + this.name.first + '.');
    };
}

let person1 = new Person('Tammi', 'Smith', 32, 'neutral', ['music', 'skiing', 'kickboxing']);
