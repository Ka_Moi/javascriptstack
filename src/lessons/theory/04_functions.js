// functions


// 1 Объявление
//=============


function greet(name) {
    console.log('Hi, ', name)
}

//expression

const greet2 = function greet2(name) {
    console.log('Hi these, ', name)
}

greet('Lena');
greet2('Lena')
console.dir(greet);


// 2 Анонимные функции
//====================


// let counter = 0
// const i = setInterval(function() {
//     if (counter === 5) {
//         clearInterval()
//     } else {
//         console.log(++counter)
//     }
// }, 1000)


// 3 Стрелочные функции
//=====================


// function greet(name) {
//     console.log(name);
// }
//
// const arrow = (name) => {
//     console.log(name);
// }
//
// const arrow2 = name => console.log(name)
// arrow2('vasya')


// const pow = num => num ** 2
// console.log(pow(2));


// 4 параметры по умолчанию
//=========================

// const sum = (a, b = a * 100 / 20) => a + b
//
// console.log(sum(42, ))

// function sumAll(...all) {
//     let result = 0;
//     for( let num of all) {
//         result += num
//     }
//     return result
// }
//
// const res = sumAll(2,3,4,5,6);
// console.log(res)


// 5 Замыкание
//============


// function createMemeber(name) {
//     return function(lastName) {
//         console.log(name + lastName);
//     }
// }

// const logWithlastName = createMemeber('Adonis ') // Замыкание функции на имени, то есть свойство - имя остается неизменным. При вызове меняется только фамилия
// console.log(logWithlastName('Zeus'))

// 6 Планирование вызова функции
//..............................

// setTimeout - вызывет функцию один раз через заданный интервал времени

// function showMessage(text, name) {
//     console.log(`${text}, ${name}`);
//     setTimeout(showMessage, 500, text, name);
// }
//
// setTimeout(showMessage, 3000, 'Привет', 'Андрей!')


// setInterval - каждый раз вызывет функцию через заданный интервал времени

// function showMessage(text, name) {
//     console.log(`${text}, ${name}`)
// }
//
// setInterval(showMessage, 500, 'Привет', 'Олеся!') //Вывод сообщения каждые пол-секунды


// Рекурсивный setInterval через зацикливание setTimeout - задержка здесь будет работать эффективнее

// function showMessage(text, name) {
//     console.log(`${text}, ${name}`);
//     setTimeout(showMessage, 500, text, name);
// }
//
// setTimeout(showMessage, 5000, 'Привет', 'Андрей!')

// Таймер с добавлением числа

// function addNumber(num) {
//     console.log(num);
//     if (num < 5) {
//         setTimeout(addNumber, 1000, ++num);
//     }
// }
//
// setTimeout(addNumber, 1000, 1)

// Clear Timeout

// function showNumber(num) {
//     console.log(num);
//     let timeId = setTimeout(showNumber, 1000, ++num);
//     if (num === 6) {
//         clearTimeout(timeId);
//     }
// }
//
// setTimeout(showNumber, 1000, 1) //  Аналогично для остановки интервала - clearInterval
