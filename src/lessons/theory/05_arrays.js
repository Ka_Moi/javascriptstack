// Массивы
// ========




// Деструктуризация массивов

// const fib = [1, 1, 3, 5, 7, 8, 13];
// const [ ,a , ,b , ,c ] = fib
//
// console.log(a, b, c)
//
//
//
// const line = [[10, 17], [14, 7]];
// const [[x1, y1], [x2, y2]] = line
//
// console.log(x1, y2)



// Деструктуризация + rest

// const people = ['chris', 'sandra', 'bob'];
// const [a, ...others] = people;
//
// console.log(others);


// const dict = {
//     duck: 'quack',
//     dog: 'woof',
//     mouse: 'squeak',
//     hamster: 'squeak',
// }
//
// const res = Object.entries(dict)
//     .filter(([, value]) => value === 'squeak')
//     .map(([key,]) => key);
// console.log(res);

// Деструктуризация массива в объекте

// const shape = {
//     type: 'segment',
//     coordinates: {
//         start: [10, 15],
//         end: [17, 15]
//     }
// };
//
// const { coordinates: {start: [x1, y1], end: [x2, y2]}} = shape
// console.log(x1, y1, x2, y2)
