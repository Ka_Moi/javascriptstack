// Объекты
//==========

// Создание объекта и его свойств

const person = {
    name: 'Vlad',      // Свойства, объявленные внутри объекта называются ключами
    age: 26,
    isProgrammer: true,
    language: ['Ru', 'En'],
    'complex key': 'Complex Value', // Символ пробела не возможен при объявлении ключей, но возможен в случае его заключения в строку
    ['key_' + (1 + 3)]: 'Computed Key',
    greet() {
        console.log('greet from person')
    },
    info() {
        console.log('this: ', this);
        console.info('Информация про человека по имени: ', this.name) // this - контекстное ключевое слово - указатель. Внутри метода будет указывать собой на объект
    }
}

console.log(person)

// Действия с объектами

// console.log(person.name); // Синтаксис обращения к свойству объекта
// const ageKey = 'age'
// console.log(person[ageKey]);
// console.log(person['age']);  // Еще один вариант синтаксиса обращения к свойству объекта
// console.log(person['complex key']);
// person.greet();
// person.age++;
// console.log(person[ageKey]);
// person.language.push('By')  // Добавляет новое значение в массив
// console.log(person.language)
// person['key_' + (1 + 3)] = undefined
// delete person['key_' + (1 + 3)]  // Удаляет ключ объекта
// console.log(person);

// Деструкторизация

// const name = person.name;
// const age = person.age;
// const languages = person.language;

// console.log(name, age, languages); // Теперь собственно - деконстракт

// const {name, age, language} = person // В скобках - свойства объекта записываются в переменные. Можно явно менять значения
// console.log(name, age, language);

// Итерация элементов объекта. Цикл for in (перебирает не только объект но и его протопип. To prevent this use .hasOwnProperty()
// for (let key in person) {
//     if (person.hasOwnProperty(key)) {
//         console.log(key)
//         console.log(person[key])
//     }
// }

// const person = {
//     firstName: 'Peter',
//     lastName: 'Smith',
//     age: 27
// }
//
// const { firstName, lastName } = person;
// console.log(firstName, lastName)

// const person = {
//     name: {
//         first: 'Peter',
//         last: 'Smith',
//     },
//     age: 27
// }
//
// const { name: {first: firstName, last: lastName} } = person;
// console.log(firstName, lastName)


// function connect({
//     host = 'localhost',
//     port = 12345,
//     user = 'guest' } ={})
//     {
//     console.log('user:', user, 'port:', port, 'host:', host);
// }
//
// connect()

// деструктуризация и rest elements (...())

// const dict = {
//     duck: 'quack',
//     dog: 'woof',
//     mouse: 'squeak'
// }
//
// const {duck, ...otherAnimals} = dict
// console.log(duck)


Object.keys(person).forEach( (key) => {
    console.log('key:', key)
    console.log('value:', person[key])
})


// Объединения ключей объектов в одну переменную методом Object.assign

// const defaults = {
//     host: 'localhost',
//     dbName: 'blog',
//     user: 'admin'
// }
// const opts = {
//     user: 'john',
//     password: 'utopia'
// }
//
// const res = Object.assign({}, defaults, opts)
// console.log(res);



// Spread оператор

// const defaults = {
//     host: 'localhost',
//     dbName: 'blog',
//     user: 'admin'
// }
// const opts = {
//     user: 'john',
//     password: 'utopia'
// }
//
// const res = { ...defaults, ...opts}
// console.log(res);


// Контекст

// person.info()

// const logger = {
//     keys() {
//         console.log('Object Keys: ', Object.keys(this))
//     },
//
//     keysAndValues() {
//         Object.keys(this).forEach( key => {
//             console.log(`"${key}": ${this[key]}`)
//         })
//     }
//
// }
// logger.keysAndValues.call(person)
