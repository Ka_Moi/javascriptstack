// const num = 42//integer
// const float = 42.42//float
// const pow = 10e3
//
// console.log('MAX_INTEGER', Number.MAX_SAFE_INTEGER)
// console.log('MAX_VALUE', Number.MAX_VALUE)
// console.log('MIN_VALUE', Number.MIN_VALUE)
// console.log('POSITIVE_INFINITY', Number.POSITIVE_INFINITY)
// console.log('NEGATIVE_INFINITY', Number.NEGATIVE_INFINITY)
// console.log('INFINITY', 1/0)
// console.log('NaN', Number.NaN)
// const weird = 2 / undefined
// console.log(Number.isNaN(weird))
// console.log(isFinite(42))  //конечно ли число 42 - true
//
// const stringInt = '42';
// const stringFloat = '42.42';
//
// //результат будет один
// console.log(Number.parseInt(stringInt) + 2)
// console.log(Number(stringInt) + 2)
// console.log(+stringInt + 2)
// console.log(Number.parseFloat(stringFloat) +2)
//
// console.log(0.4 + 0.2)
// console.log(parseFloat((0.4 + 0.2).toFixed(1)))
//
// //BigInt
//
// // console.log(parseInt(10n) -4)
// console.log(10n - BigInt(4))
// console.log(5n / 3n)
//
// //Math
//
// console.log(Math.E)
// console.log(Math.PI)
//
// console.log(Math.sqrt(25))
// console.log('возведение в степень', Math.pow(5, 3))
// console.log(Math.abs(-42))
// console.log('Округлятор в меньшую сторону', Math.floor(4.6))
// console.log('Округлятор в большую сторону', Math.ceil(4.3))


//Example

// function getRandomBetween(min, max) {
//     return Math.floor(Math.random() * (max - min + 1) + min)
// }
//
// console.log(getRandomBetween(10, 42));
