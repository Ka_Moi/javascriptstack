const modal = $.modal({
    title: 'Title',
    closable: true,
    content: `
        <h4>Modal is working</h4>
        <p>Lorem ipsum dolor sit amet.</p>
    `,
    width: '400px',
    footerButtons: [
        {text: 'Ok', type: 'primary', handler() {
            console.log('primary clicked')
            modal.close()
        }},
        {text: 'Cancel', type: 'danger', handler() {
            console.log('danger clicked')
            modal.close()
        }}
    ]
})
