function initFilters() {
    const products = document.querySelectorAll('.product')

    const filterData = [];

    products.forEach((item) => {
        const changingData = Object.keys(item.dataset)

        changingData.forEach((name) => {
            let existingObject = filterData.find(item => item.name === name)

            if (!existingObject) {
                existingObject = {
                    name: name,
                    values: [],
                    selectedValue: ''
                };

                filterData.push(existingObject);
            }

            if (!existingObject.values.includes(item.dataset[name])) {
                existingObject.values.push(item.dataset[name]);
            }
        })
    });

    function renderFilters (filterData) {
        let result = '<div>';
        filterData.forEach((filterItem) => {
            result += `<div>
                   <label for="${filterItem.name}">${filterItem.name}</label>
                   <select class="filter-select" id="${filterItem.name}">
                   <option value=''></option>`

            filterItem.values.forEach((value) => {
                result += `<option value="${value}">${value}</option>`
            })

            result += `</select></div>`
        })

        result += `</div>`

        const filters = document.querySelector('.navigation')
        filters.innerHTML = result;
    }

    renderFilters(filterData);

    const selectInputs = document.querySelectorAll('.filter-select');
    selectInputs.forEach((select) => {
        select.addEventListener('change', () => {
            const dataItem = filterData.find(filterDataItem => {
                return filterDataItem.name === select.id;
            })
            dataItem.selectedValue = select.value;
            document.dispatchEvent(new Event('filterDataChanged'));
        });
    })

    function doFilter() {
        products.forEach((product) => {
            let isMatched = true;
            Object.keys(product.dataset).forEach((item) => {
                const productPropertyValue = product.dataset[item];
                const filterItem = filterData.find(filterDataItem => filterDataItem.name === item);
                const filterValue = filterItem.selectedValue

                if (filterValue !== '' && filterValue !== productPropertyValue) {
                    isMatched = false;
                }
            })
            if (isMatched === true) {
                product.style.display = 'block'
            } else {
                product.style.display = 'none'
            }
        })
    }

    document.addEventListener('filterDataChanged', () => {
        doFilter()
    })
}


