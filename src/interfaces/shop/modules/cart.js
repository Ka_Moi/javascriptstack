class Cart {
    constructor() {
        this.items = [];
        this.totalCount = 0;
        this.totalPriceSum = 0;
    }

    updateCartTotals(cartItem) {
        this.totalCount = this.items.length;
        this.totalPriceSum = 0;
        this.items.forEach((elem) => {
            this.totalPriceSum += elem.price * elem.amount;
        })
    }

    addCartItem(cartItem) {
        this.items.push(cartItem);
        this.updateCartTotals();
        this.addStorageCart()
    };

    removeCartItem(id) {
        const elIndex = this.items.findIndex(cartItem => cartItem.id === id);
        this.items.splice(elIndex, 1);
        this.updateCartTotals();
        this.removeStorageCart()
    };

    clearCart() {
        this.items = []
    }

    getCartItemByID(id) {
        return this.items.find(function (item) {
            return id === item.id
        })
    }

    updateCartItemAmount(id, amount) {
        const item = this.getCartItemByID(id);
        item.amount = amount
        this.updateCartTotals();
    }

    renderCart() {
        console.log(this)
    }

    addStorageCart() {
        let myStorage = window.localStorage;
        myStorage.setItem('Cart', JSON.stringify(this));
    }

    removeStorageCart() {
        let myStorage = window.localStorage;
        myStorage.removeItem('Cart')
    }
}

class CartItem {
    constructor(name, price, amount,id) {
        this.name = name;
        this.price = price;
        this.amount = amount;
        this.id = (+new Date)
    }
}
