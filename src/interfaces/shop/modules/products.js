async function getProductsFromServer() {
    let response = await fetch('https://fakestoreapi.com/products');
    return await response.json()
}

function renderProductList(products) {
    const div = document.querySelector('#div');
    products.forEach((product) => {
        let template = document.querySelector('#template');
        let templateString = template.innerHTML;
        templateString = templateString.replaceAll('%productImage%', product.image);
        templateString = templateString.replaceAll('%productTitle%', product.title);
        templateString = templateString.replaceAll('%productPrice%', product.price);
        templateString = templateString.replaceAll('%productCategory%', product.category);

        const newEl = document.createElement('div')
        div.appendChild(newEl);

        newEl.outerHTML = templateString
    })
}
