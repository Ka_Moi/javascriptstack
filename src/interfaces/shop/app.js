(async () => {
    const cart = new Cart();

    const products = await getProductsFromServer();
    renderProductList(products);
    initFilters();

    const buttons = document.querySelectorAll('.add-to-cart');
    buttons.forEach( (btn) => {
        btn.addEventListener('click', () => {
            const cartItemToAdd = new CartItem( btn.dataset.name, parseFloat(btn.dataset.price), 1);
            cart.addCartItem(cartItemToAdd);

            console.log(cart)
        })
    })
}) ();
