new Swiper('.image-slider', {

    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev'
    },

    autoplay: {
        delay: 3000,
        pauseOnMouseEnter: true
    },

    disableOnInteraction: false,
    loop: true,
    speed: 600
})

Swiper.scrollbar.destroy()
