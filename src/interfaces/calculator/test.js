function calculating(a, operation, b) {
    if (operation === '+') {
        return a + b;
    }
    if (operation === '-') {
        return a - b;
    }
    if (operation === '*') {
        return a * b;
    }
    if (operation === '/') {
        return a / b;
    }

    return null
}

const form = document.querySelector('#getForm');
const output = form.querySelector('#output')

// Вывод по кнопке
//================
// if (form) {
//     form.addEventListener("submit", function(event) {
//         event.preventDefault();
//         const operand1 = form.querySelector('#operand1');
//         const operand2 = form.querySelector('#operand2');
//         const operator = form.querySelector('#select');
//
//         output.innerText = calculating(parseInt(operand1.value), operator.value, parseInt(operand2.value) );
//
//
//     })
// }

// Вывод по заполнению полей ввода
//================================
const input1 = form.querySelector('#operand1')
const selector = form.querySelector('#select')
const input2 = form.querySelector('#operand2')

function getNumber() {
    output.innerText = '';
    if(parseInt(input1.value) && selector.value && parseInt(input2.value)) {
        output.innerText  = calculating(parseInt(input1.value), selector.value, parseInt(input2.value));
    }
}

input1.addEventListener('input', getNumber);
selector.addEventListener('change', getNumber);
input2.addEventListener('input', getNumber);
console.log()
