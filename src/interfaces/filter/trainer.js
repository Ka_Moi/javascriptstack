const getInput = document.getElementById('input');

getInput.addEventListener("keyup", filterInput);

function filterInput() {
    let filter = document.getElementById('input').value.toUpperCase();
    let filterNames = document.getElementById('names');
    let filterItem = filterNames.querySelectorAll('li.item');



    filterItem.forEach(li => {
        let linkItem = li.getElementsByTagName('a')[0];
        if (linkItem.innerText.toUpperCase().includes(filter)) {
            li.style.display = '';
        } else {
            li.style.display = 'none';
        }
    })


    for(let i = 0; i < filterItem.length; i++) {
        let linkItem = filterItem[i].getElementsByTagName('a')[0];
        if(linkItem.innerText.toUpperCase().indexOf(filter) > -1) {
            filterItem[i].style.display = '';
        } else {
            filterItem[i].style.display = 'none';
        }
    }
}
