const Data = {
  name: "name",
  age: 1,
  address: {
      country: "country",
      region: "region",
      city: "city",
      street: "street",
      number: 1,
      index: 12
  },
  tel: 13213123123123,
  email: "email",
  hobbies: [
    1,
    2,
    3,
    4,
    5
  ]
}
