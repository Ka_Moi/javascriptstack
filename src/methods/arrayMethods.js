// Array.prototype.filter()
// //Returns a new array containing all elements of the calling array for
// //which the provided filtering function returns true
//
// Array.prototype.find()
// //Returns the value of the first element in the array that satisfies the provided testing function,
// // or undefined if no appropriate element is found
//
// Array.prototype.findIndex()
// //Returns the index of the first element in the array that satisfies the provided testing
// // function, or -1 if no appropriate element was found
//
// Array.prototype.includes()
// //Determines whether the calling array contains a value, returning true or false as appropriate
//
// Array.prototype.indexOf()
// //Returns the first (least) index at which a given element can be found in the calling array.
//
// Array.prototype.join()
// //Joins all elements of an array into a string
//
// Array.prototype.keys()
// //Returns a new array iterator that contains the keys for each index in the calling array
//
// Array.prototype.map()
// //Returns a new array containing the results of invoking a function on every element in the calling array
//
// Array.prototype.push()
// //Adds one or more elements to the end of an array, and returns the new length of the array
//
// Array.prototype.reduce()
// //Executes a user-supplied "reducer" callback function on each element of the array (from left to right), to reduce it to a single value
//
// Array.prototype.reverse()
// //Reverses the order of the elements of an array in place. (First becomes the last, last becomes first.)
//
// Array.prototype.shift()
// // Removes the first element from an array and returns that element
//
// Array.prototype.slice()
// // Extracts a section of the calling array and returns a new array
//
// Array.prototype.sort()
// // Sorts the elements of an array in place and returns the array
//
// Array.prototype.splice()
// // Adds and/or removes elements from an array
//
// Array.prototype.toString()
// // Returns a string representing the calling array and its elements. Overrides the Object.prototype.toString() method
//
// Array.prototype.unshift()
// // Adds one or more elements to the front of an array, and returns the new length of the array
